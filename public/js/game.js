/* eslint-disable no-restricted-syntax */
/* eslint-disable no-shadow */
/* eslint-env browser */

// // Variables

// const player1Name = data.player1Foreign.name
// let player2Name
// let player1Choice
// let player2Choice
// let gameResult

// const roundNumberText = document.querySelector('.round__number')
// const versusText = document.querySelector('.divider__versus')
// const statusWinText = document.querySelector('.divider__win')
// const statusLoseText = document.querySelector('.divider__lose')
// const statusDrawText = document.querySelector('.divider__draw')
// const refreshBtn = document.querySelector('.refresh__btn')

// // Functions

// const showResults = () => {
//   const userItems = document.getElementsByClassName('user__item')
//   for (const item of userItems) {
//     item.classList.remove('available__item')
//   }

//   versusText.classList.add('hidden')
//   if (gameResult === 'win') {
//     statusWinText.classList.remove('hidden')
//     document.getElementsByClassName(`${player1Choice} user__item`)[0].classList.add('item__win')
//     document.getElementsByClassName(`${player2Choice} bot__item`)[0].classList.add('item__lose')
//   } else if (gameResult === 'lose') {
//     statusLoseText.classList.remove('hidden')
//     document.getElementsByClassName(`${player1Choice} user__item`)[0].classList.add('item__lose')
//     document.getElementsByClassName(`${player2Choice} bot__item`)[0].classList.add('item__win')
//   } else {
//     statusDrawText.classList.remove('hidden')
//     document.getElementsByClassName(`${player1Choice} user__item`)[0].classList.add('item__draw')
//     document.getElementsByClassName(`${player2Choice} bot__item`)[0].classList.add('item__draw')
//   }
//   refreshBtn.classList.remove('hidden')
// }

// const clearResults = () => {
//   const userItems = document.getElementsByClassName('user__item')
//   const allItems = document.getElementsByClassName('item')

//   for (const item of userItems) {
//     item.classList.add('available__item')
//   }

//   for (const item of allItems) {
//     item.classList.remove('item__win', 'item__lose', 'item__draw')
//   }

//   versusText.classList.remove('hidden')
//   statusWinText.classList.add('hidden')
//   statusLoseText.classList.add('hidden')
//   statusDrawText.classList.add('hidden')
//   refreshBtn.classList.add('hidden')
// }

// // API

// const requestStartGame = async (choice) => {
//   const options = {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json',
//     },
//     body: JSON.stringify({ choice }),
//   }

//   await fetch('api/v1/game/start', options)
//     .then((response) => response.json())
//     .then((result) => {
//       console.log(result)
//       gameResult = result.result
//       player1Choice = result.player1Choice
//       player2Choice = result.player2Choice
//     })
//     .catch((error) => console.log('error', error))
// }

// const requestRefreshGame = async () => {
//   const options = {
//     method: 'GET',
//   }

//   await fetch('api/v1/game/refresh', options)
//     .then((response) => response.json())
//     .then((result) => {
//       console.log(result)
//       gameResult = result.result
//       player1Choice = result.player1Choice
//       player2Choice = result.player2Choice
//     })
//     .catch((error) => console.log('error', error))
// }

// // DOM

// document.getElementById('rock').addEventListener('click', async () => {
//   if (!gameResult && player2Name && player2Name) {
//     await requestStartGame('rock')
//     showResults()
//   }
// })

// document.getElementById('paper').addEventListener('click', async () => {
//   if (!gameResult && player2Name) {
//     await requestStartGame('paper')
//     showResults()
//   }
// })

// document.getElementById('scissors').addEventListener('click', async () => {
//   if (!gameResult && player2Name) {
//     await requestStartGame('scissors')
//     showResults()
//   }
// })

// document.getElementById('refresh').addEventListener('click', async () => {
//   clearResults()
//   await requestRefreshGame()
// })
