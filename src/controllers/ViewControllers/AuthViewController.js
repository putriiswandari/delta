import fetch from 'node-fetch'

class AuthViewController {
  static getLoginPage = (req, res) => res.status(200).render('login')

  static getRegisterPage = (req, res) => res.status(200).render('register')

  static loginUser = (req, res) => {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(req.body),
    }

    fetch(`${process.env.APP_URL}/api/v1/auth/login`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        res.status(200).json(result)
      })
      .catch(() => res.status(500).json({ message: 'Internal Server Error' }))
  }

  static registerNewUser = (req, res) => {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${req.cookies.token}`,
      },
      body: JSON.stringify(req.body),
    }

    fetch(`${process.env.APP_URL}/api/v1/user`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        res.redirect('/auth/login')
      })
      .catch(() => res.status(500).json({ message: 'Internal Server Error' }))
  }
}
export default AuthViewController
