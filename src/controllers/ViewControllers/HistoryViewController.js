import fetch from 'node-fetch'
import { Room, User } from '../../models'

class AuthViewController {
  static get = (req, res) => {
    Room.findAll({
      where: {
        status: 'finished'
      },
      include: [
        {
          model: User,
          as: 'player1Foreign',
          attributes: ['name', 'user_name'],
        },
        {
          model: User,
          as: 'player2Foreign',
          attributes: ['name', 'user_name'],
        },
      ],
      order: [
        ['updatedAt', 'DESC'],
      ],
    })
      .then((result) => {
        res.status(200).render('history', { decoded: req.decoded, data: result })
      })
  }
}
export default AuthViewController
