import fetch from 'node-fetch'

class RoomViewController {
  static getAllRoom = (req, res) => {
    const requestOptions = {
      method: 'GET',
      headers: { Authorization: `Bearer ${req.cookies.token}` },
      redirect: 'follow',
    }

    fetch(`${process.env.APP_URL}/api/v1/room`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        res.status(200).render('room', { data: result, decoded: req.decoded })
      })
      .catch(() => res.status(500).json({ message: 'Internal Server Error' }))
  }

  static createRoom = (req, res) => {
    const { roomName, userId } = req.body

    const requestOptions = {
      method: 'POST',
      headers: { Authorization: `Bearer ${req.cookies.token}`, 'Content-Type': 'application/json' },
      body: JSON.stringify({ roomName, userId }),
      redirect: 'follow',
    };

    fetch(`${process.env.APP_URL}/api/v1/room`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        res.status(200).render('room', { data: result, decoded: req.decoded })
      })
      .catch(() => res.status(500).json({ message: 'Internal Server Error' }));
  }

  static getOneRoom = (req, res) => {
    const { roomName } = req.params

    const requestOptions = {
      method: 'GET',
      headers: { Authorization: `Bearer ${req.cookies.token}` },
      redirect: 'follow',
    }

    fetch(`${process.env.APP_URL}/api/v1/room/${roomName}`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        res.status(200).render('gameRoom', { data: result, decoded: req.decoded })
      })
      .catch(() => res.status(500).json({ message: 'Internal Server Error' }))
  }
}
export default RoomViewController
