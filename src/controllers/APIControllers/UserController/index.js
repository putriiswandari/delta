import { User } from '../../../models'

class UserController {
  static get = (req, res) => User.findAll().then(
    (user) => res.status(200).json(user),
  ).catch(
    (e) => {
      console.log(e)
      res.status(500).json(e)
    },
  )

  static create = (req, res) => {
    const {
      // eslint-disable-next-line camelcase
      name, email, password, user_name,
    } = req.body

    User.findOne({
      where: { email },
    }).then(
      (user) => {
        if (user) {
          return res.status(400).json({ status: 400, message: 'Email already exist' })
        }

        return User.create({
          name,
          email,
          password: User.hashPassword(password),
          user_name,
        }).then(
          (data) => res.status(201).json({ ...data.dataValues }),
        ).catch(
          (e) => {
            console.log(e)
            return res.status(500).json(e)
          },
        )
      },
    ).catch(
      (e) => {
        console.log(e)
        res.status(500).json(e)
      },
    )
  }

  static update = (req, res) => {
    const { id } = req.params

    return User.findOne({
      where: { id },
    }).then(
      (user) => {
        if (!user) return res.status(404).json({ message: 'Not found' })

        const {
          // eslint-disable-next-line camelcase
          name, email, password, user_name,
        } = req.body

        return user.update(
          {
            name,
            email,
            password: User.hashPassword(password),
            user_name,
          },
        ).then(
          (updated) => res.status(200).json({ ...updated.dataValues }),
        )
      },
    ).catch(
      (e) => {
        console.log(e)
        res.status(500).json(e)
      },
    )
  }

  static delete = (req, res) => {
    const { id } = req.params

    return User.destroy({
      where: { id },
    }).then(
      (user) => {
        if (!user) return res.status(404).json({ message: 'Not found' })

        return res.status(200).json({ message: 'Deleted' })
      },
    ).catch(
      (e) => {
        console.log(e)
        res.status(500).json(e)
      },
    )
  }
}

export default UserController
