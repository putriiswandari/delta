import bcrypt from 'bcrypt'
import { User } from '../../../models'
import JwtAuth from '../../../utils/JwtAuth'

class AuthController {
  static login = (req, res) => {
    const { email, password } = req.body

    return User.findOne({
      where: {
        email,
      },
    }).then(
      (user) => {
        if (user && bcrypt.compareSync(password, user.password)) {
          const { id, name } = user

          return JwtAuth.sign(
            { id, name, email },
            (token) => res.status(200).json({ token }),
          )
        }

        return res.status(401).json({ message: 'Wrong email or password combination' })
      },
    ).catch(
      () => res.status(500).json({ message: 'Internal Server Error' }),
    )
  }
}

export default AuthController
