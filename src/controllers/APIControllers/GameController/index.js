import fetch from 'node-fetch'
import game from '../../../engine'

class GameController {
  static choose = (req, res) => {
    // console.log(req.body)
    const { choice, role, roomName } = req.body
    let payload

    if (!choice && !role) {
      res.status(400).json({
        status: 400,
        message: 'bad input',
      })
    }

    if (role === 1) {
      payload = {
        player1_choice: choice,
      }
    } else {
      payload = {
        player2_choice: choice,
      }
    }

    const requestOptions = {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${req.cookies.token}`,
      },
      body: JSON.stringify(payload),
      redirect: 'follow',
    }

    fetch(`${process.env.APP_URL}/api/v1/room/${roomName}`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        res.status(200).json({
          status: 200,
          message: 'success',
          response: result,
        })
      })
      .catch(() => res.status(500).json({ message: 'Internal Server Error' }))
  }

  static result = async (req, res) => {
    const { player1_choice, player2_choice, roomName } = req.body
    let player1_result
    let player2_result

    if (player1_choice === player2_choice) {
      player1_result = 'draw'
      player2_result = 'draw'
    } else if ((player1_choice === 'rock' && player2_choice === 'scissors') || (player1_choice === 'paper' && player2_choice === 'rock') || (player1_choice === 'scissors' && player2_choice === 'paper')) {
      player1_result = 'win'
      player2_result = 'lose'
    } else if ((player1_choice === 'rock' && player2_choice === 'paper') || (player1_choice === 'paper' && player2_choice === 'scissors') || (player1_choice === 'scissors' && player2_choice === 'rock')) {
      player1_result = 'lose'
      player2_result = 'win'
    } else {
      player1_result = 'invalid'
      player2_result = 'invalid'
    }

    const requestOptions = {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${req.cookies.token}`,
      },
      body: JSON.stringify({ player1_result, player2_result, status: 'finished' }),
      redirect: 'follow',
    };

    fetch(`${process.env.APP_URL}/api/v1/room/${roomName}`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        res.status(200).json({
          status: 200,
          message: 'success',
          player1_choice,
          player2_choice,
          player1_result,
          player2_result,
        })
      })
      .catch(() => res.status(500).json({ message: 'Internal Server Error' }));
  }

  static refresh = (req, res) => {
    game.refreshGame()
    res.status(200).json({
      status: 200,
      message: 'success',
      refresh: true,
      player1Choice: game.getPlayer1().getChoice(),
      player2Choice: game.getPlayer2().getChoice(),
      result: game.getResult(),
    })
  }
}

export default GameController
