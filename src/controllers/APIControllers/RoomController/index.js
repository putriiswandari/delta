import { Room, User } from '../../../models'

class RoomController {
  static get = (req, res) => Room.findAll({
    include: [
      {
        model: User,
        as: 'player1Foreign',
        attributes: ['name', 'user_name'],
      },
      {
        model: User,
        as: 'player2Foreign',
        attributes: ['name', 'user_name'],
      },
    ],
    order: [
      ['createdAt'],
    ],
  }).then(
    (room) => res.status(200).json(room),
  ).catch(
    (e) => {
      console.log(e)
      res.status(500).json({ message: e })
    },
  )

  static getByRoomName = (req, res) => {
    const { roomName } = req.params

    Room.findOne({
      where: { roomName },
      include: [
        {
          model: User,
          as: 'player1Foreign',
          attributes: ['name', 'user_name'],
        },
        {
          model: User,
          as: 'player2Foreign',
          attributes: ['name', 'user_name'],
        },
      ],
    }).then(
      (room) => res.status(200).json(room),
    ).catch(
      (e) => {
        console.log(e)
        res.status(500).json({ message: e })
      },
    )
  }

  static create = (req, res) => {
    const { roomName, userId } = req.body

    Room.findOne({
      where: { roomName },
    }).then(
      (room) => {
        if (room) {
          return res.status(400).json({ status: 400, message: 'RoomName already exist' })
        }

        return Room.create({
          roomName,
          player1: userId,
          status: 'waiting for player 2',
        }).then(
          (data) => res.status(201).json({ ...data.dataValues }),
        ).catch(
          (e) => {
            console.log(e)
            res.status(500).json({ message: e })
          },
        )
      },
    ).catch(
      (e) => {
        console.log(e)
        res.status(500).json(e)
      },
    )
  }

  static update = (req, res) => {
    const { roomName } = req.params

    return Room.findOne({
      where: { roomName },
    }).then(
      (room) => {
        if (!room) return res.status(404).json({ message: 'Not found' })

        const {
          player1,
          player2,
          player1_choice,
          player2_choice,
          player1_result,
          player2_result,
          status,
        } = req.body

        return room.update(
          {
            player1,
            player2,
            player1_choice,
            player2_choice,
            player1_result,
            player2_result,
            status,
          },
        ).then(
          (updated) => res.status(200).json({ ...updated.dataValues }),
        )
      },
    ).catch(
      (e) => {
        console.log(e)
        res.status(500).json({ message: e })
      },
    )
  }

  static delete = (req, res) => {
    const { roomName } = req.params

    return Room.destroy({
      where: { roomName },
    }).then(
      (room) => {
        if (!room) return res.status(404).json({ message: 'Not found' })

        return res.status(200).json({ message: 'Deleted' })
      },
    ).catch(
      (e) => {
        console.log(e)
        res.status(500).json({ message: e })
      },
    )
  }
}

export default RoomController
