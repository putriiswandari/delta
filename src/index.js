import express from 'express'
import bodyParser from 'body-parser'
import methodOverride from 'method-override'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import cors from 'cors'
import swaggerUi from 'swagger-ui-express'
import swaggerDocument from './openapi.json'
import routes from './routes'
import db from './models'

require('dotenv').config()

db.sequelize.authenticate()
  .then(() => console.log(`Database connected on port:${process.env.DB_PORT}`))
  .catch((err) => console.log(`Error: ${err}`))

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(methodOverride('_method'))

app.use(logger('dev'))
app.use(cookieParser())

app.use(cors())
app.use(express.json())
app.use(express.static('public'))

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.set('view engine', 'ejs')
app.set('views', 'src/views')

app.use('/', routes)

app.listen(3001, () => console.log(`Running on ${process.env.APP_URL}`))
