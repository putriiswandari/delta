module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Room', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      roomName: {
        type: Sequelize.STRING,
      },
      player1: {
        type: Sequelize.UUID,
        onDelete: 'cascade',
        references: {
          model: 'User',
          key: 'id',
        },
      },
      player2: {
        type: Sequelize.UUID,
        onDelete: 'cascade',
        references: {
          model: 'User',
          key: 'id',
        },
      },
      player1_choice: {
        type: Sequelize.STRING,
      },
      player2_choice: {
        type: Sequelize.STRING,
      },
      player1_result: {
        type: Sequelize.STRING,
      },
      player2_result: {
        type: Sequelize.STRING,
      },
      status: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Room');
  },
};
