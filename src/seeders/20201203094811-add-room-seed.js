module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Room', [
      {
        id: 'f5110273-abb3-4c77-9828-031b96f61df6',
        roomName: '',
        player1: null,
        player2: null,
        player1_choice: '',
        player2_choice: '',
        player1_result: '',
        player2_result: '',
        status: '',
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        id: 'c5026f4d-2459-4b94-9b09-ada4e924989c',
        roomName: '',
        player1: null,
        player2: null,
        player1_choice: '',
        player2_choice: '',
        player1_result: '',
        player2_result: '',
        status: '',
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        id: '0dcef496-44fc-4d18-92ad-ef08a5c68d96',
        roomName: '',
        player1: null,
        player2: null,
        player1_choice: '',
        player2_choice: '',
        player1_result: '',
        player2_result: '',
        status: '',
        createdAt: new Date(),
        updatedAt: new Date(),
      }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Room', null, {});
  },
};
