import { body, param } from 'express-validator'

class AuthValidator {
  static login = [
    body('email').exists().isString(),
    body('password').exists().isString(),
  ]
}

export default AuthValidator
