import { body, param } from 'express-validator'

class HistoryValidator {
  static create = () => [
    body('score').exists().isString(),
    body('time').exists().isString(),
  ]

  static update = () => [
    param('id').exists().isUUID(),
    body('score').exists().isString(),
    body('time').exists().isString(),
  ]

  static delete = () => [
    param('id').exists().isUUID(),
  ]
}

export default HistoryValidator
