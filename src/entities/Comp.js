/* eslint-disable no-useless-constructor */
import Player from './Player'

class Comp extends Player {
  constructor(name) {
    super(name)
  }

  randomizeChoice = (items) => {
    const randomItem = items[Math.floor(Math.random() * 3)]
    return this.setChoice(randomItem)
  }
}

export default Comp
