class Player {
  constructor(name) {
    this.name = name
    this.choice = null
  }

  clearChoice = () => {
    if (this.choice) {
      this.choice = null
    }
  }

  getName = () => this.name

  getChoice = () => this.choice

  setName = (name) => {
    this.name = name
  }

  setChoice = (item) => {
    this.choice = item
  }
}

export default Player
