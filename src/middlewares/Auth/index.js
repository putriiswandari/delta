import JwtAuth from '../../utils/JwtAuth'

export const BasicAuth = (req, res, next) => {
  const { headers: { authorization } } = req
  // Basic hasgtdiusadhss=  ==> ['Basic', 'hasgtdiusadhss=']

  const b64auth = (authorization || '').split(' ')[1] || ''
  const [username, password] = Buffer.from(b64auth, 'base64').toString().split(':')
  // username:password => ['username', 'password']

  // 123 => bcrypt => jsadosa798jkasjd98a

  console.log(username, password)
  // TODO: Validate User

  return next()
}

export const AuthMiddleware = (req, res, next) => {
  console.log(req.headers)

  const { headers: { authorization = null } } = req

  if (!authorization) return res.status(401).json({ message: 'Unauthorized' })

  const token = authorization.split(' ')[1]

  return JwtAuth.verify(
    token,
    (decoded) => {
      req.decoded = decoded

      return next()
    },
    () => res.status(401).json({ message: 'Unauthorized' }),
  )
}

export const AuthViewMiddleware = (req, res, next) => {
  const { cookies: { token = null } } = req

  if (!token) return res.redirect('/auth/login')

  return JwtAuth.verify(
    token,
    (decoded) => {
      req.decoded = decoded

      return next()
    },
    () => res.redirect('/auth/login'),
  )
}
