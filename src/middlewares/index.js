import { validationResult } from 'express-validator'
import { AuthMiddleware, AuthViewMiddleware } from './Auth'

class Middleware {
  static CheckGuest = (req, res, next) => this.handler('checkGuest', req, res, next)

  static Guest = (req, res, next) => this.handler('guest', req, res, next)

  static Auth = (req, res, next) => this.handler('auth', req, res, next)

  static AuthView = (req, res, next) => this.handler('authView', req, res, next)

  static User = () => { }

  static handler = (type, req, res, next) => {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }

    switch (type) {
      case 'guest': return next()
      case 'checkGuest': return (req.cookies.token) ? AuthViewMiddleware(req, res, next) : next()
      case 'auth': return AuthMiddleware(req, res, next)
      case 'authView': return AuthViewMiddleware(req, res, next)
      default: return res.status(403)
    }
  }
}

export default Middleware
