import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    static associate(models) {
      const { User } = models

      Room.belongsTo(User, { foreignKey: 'player1', as: 'player1Foreign' })
      Room.belongsTo(User, { foreignKey: 'player2', as: 'player2Foreign' })
    }
  }
  Room.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
      unique: true,
      defaultValue: DataTypes.UUIDV4,
    },
    roomName: DataTypes.STRING,
    player1: DataTypes.UUID,
    player2: DataTypes.UUID,
    player1_choice: DataTypes.STRING,
    player2_choice: DataTypes.STRING,
    player1_result: DataTypes.STRING,
    player2_result: DataTypes.STRING,
    status: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Room',
    timestamps: true,
  });
  return Room;
};
