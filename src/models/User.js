import bcrypt from 'bcrypt'
import { Model } from 'sequelize'

const salt = bcrypt.genSaltSync()

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static hashPassword = (password) => bcrypt.hashSync(password, salt)

    static associate(models) {
      const { History, Room } = models

      User.hasMany(History, { foreignKey: 'id' })
      User.hasMany(Room, { foreignKey: 'id' })
    }
  }
  User.init({
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    user_name: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'User',
    timestamps: true,
  })
  return User
}
