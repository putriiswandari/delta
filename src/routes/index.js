import express from 'express'
import api from './api'
import views from './views'

const router = express.Router()

router.use('/', views)
router.use('/api', api)
router.use((req, res) => res.render('404'))

export default router
