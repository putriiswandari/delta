import express from 'express'
import userRoutes from './user'
import roomRoutes from './room'
import gameRoutes from './game'
import authRoutes from './auth'

const router = express.Router()

router.use('/user', userRoutes)
router.use('/room', roomRoutes)
router.use('/game', gameRoutes)
router.use('/auth', authRoutes)

export default router
