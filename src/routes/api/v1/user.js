import express from 'express'
import UserController from '../../../controllers/APIControllers/UserController'
import Middleware from '../../../middlewares'

const router = express.Router()

router.get('/', Middleware.Auth, UserController.get)
router.post('/', Middleware.Guest, UserController.create)
router.patch('/:id', Middleware.Auth, UserController.update)
router.delete('/:id', Middleware.Auth, UserController.delete)

export default router
