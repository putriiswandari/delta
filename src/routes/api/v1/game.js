import express from 'express'
import Middleware from '../../../middlewares'
import GameController from '../../../controllers/APIControllers/GameController'

const router = express.Router()

router.post('/choose', Middleware.Auth, GameController.choose)
router.post('/result', Middleware.Auth, GameController.result)
router.get('/refresh', Middleware.Auth, GameController.refresh)

export default router
