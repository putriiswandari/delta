import express from 'express'
import Middleware from '../../../middlewares'
import RoomValidator from '../../../validators/RoomValidator'
import RoomController from '../../../controllers/APIControllers/RoomController'

const router = express.Router()

router.get('/', Middleware.Auth, RoomController.get)
router.get('/:roomName', Middleware.Auth, RoomController.getByRoomName)
router.post('/', [RoomValidator.create, Middleware.Auth], RoomController.create)
router.patch('/:roomName', Middleware.Auth, RoomController.update)
router.delete('/:roomName', Middleware.Auth, RoomController.delete)

export default router
