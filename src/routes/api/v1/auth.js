import express from 'express'
import Middleware from '../../../middlewares'
import AuthController from '../../../controllers/APIControllers/AuthController'
import AuthValidator from '../../../validators/AuthValidator'

const router = express.Router()

router.post('/login', [AuthValidator.login, Middleware.Guest], AuthController.login)

export default router
