import express from 'express'
import RoomViewController from '../../controllers/ViewControllers/RoomViewController'
import Middleware from '../../middlewares'

const router = express.Router()

router.get('/', Middleware.AuthView, RoomViewController.getAllRoom)
router.get('/:roomName', Middleware.AuthView, RoomViewController.getOneRoom)
router.post('/', Middleware.AuthView, RoomViewController.createRoom)

export default router
