import express from 'express'
import HistoryViewController from '../../controllers/ViewControllers/HistoryViewController'
import Middleware from '../../middlewares'

const router = express.Router()

router.get('/', Middleware.AuthView, HistoryViewController.get)

export default router
