import express from 'express'
import Middleware from '../../middlewares'
import authRoutes from './authRoutes'
import roomRoutes from './roomRoutes'
import historyRoutes from './historyRoutes'

const router = express.Router()

router.get('/', Middleware.CheckGuest, (req, res) => res.status(200).render('landing', { decoded: req.decoded }))
router.use('/auth', authRoutes)
router.use('/room', roomRoutes)
router.get('/profile', Middleware.AuthView, (req, res) => res.status(200).render('profile', { decoded: req.decoded }))
router.use('/history', historyRoutes)

router.get('/token', (req, res) => {
  // const token = 'aaa'
  res.send(req.cookies)
})

export default router
