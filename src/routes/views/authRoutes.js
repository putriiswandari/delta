import express from 'express'
import AuthViewController from '../../controllers/ViewControllers/AuthViewController'
import Middleware from '../../middlewares'

const router = express.Router()

router.get('/login', Middleware.Guest, AuthViewController.getLoginPage)
router.post('/login', Middleware.Guest, AuthViewController.loginUser)

router.get('/register', Middleware.Guest, AuthViewController.getRegisterPage)
router.post('/register', Middleware.Guest, AuthViewController.registerNewUser)

router.get('/logout', Middleware.AuthView, (req, res) => {
  res.redirect('/auth/login')
})

export default router
